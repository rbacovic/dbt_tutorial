
set dbtut_role = '{dbtut_role}';
set dbtut_username = '{dbtut_username}';
set dbtut_warehouse = '{dbtut_warehouse}';
set dbtut_database = '{dbtut_database}';
set dbtut_schema = '{dbtut_schema}';

-- set user password
set dbtut_password ='{dbtut_password}';

begin;

-- create  role
use role securityadmin;
create role if not exists identifier($dbtut_role);
grant role identifier($dbtut_role) to role SYSADMIN;

-- create  user
create user if not exists identifier($dbtut_username)
password = $dbtut_password
default_role = $dbtut_role
default_warehouse = $dbtut_warehouse;

grant role identifier($dbtut_role) to user identifier($dbtut_username);

-- change role to sysadmin for warehouse/database steps
use role sysadmin;

-- create  warehouse
create warehouse if not exists identifier($dbtut_warehouse)
warehouse_size = xsmall
warehouse_type = standard
auto_suspend = 60
auto_resume = true
initially_suspended = true;

-- create  database
create database if not exists identifier($dbtut_database);

-- grant  warehouse access
grant USAGE
on warehouse identifier($dbtut_warehouse)
to role identifier($dbtut_role);

-- grant  database access
grant OWNERSHIP
on database identifier($dbtut_database)
to role identifier($dbtut_role);

commit;

begin;

USE DATABASE identifier($dbtut_database);

-- create schema for data
CREATE SCHEMA IF NOT EXISTS identifier($dbtut_schema);

commit;

begin;

-- grant schema access
grant OWNERSHIP
on schema identifier($dbtut_schema)
to role identifier($dbtut_role);

commit;