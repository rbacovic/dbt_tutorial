CREATE OR REPLACE DATABASE DBT_TUTORIAL_DB;
CREATE OR REPLACE SCHEMA DBT_TUTORIAL_DB.RAW;

CREATE OR REPLACE TABLE dbt_tutorial_db.raw.customer AS SELECT * FROM SNOWFLAKE_SAMPLE_DATA.tpch_sf10.customer;
CREATE OR REPLACE TABLE dbt_tutorial_db.raw.nation   AS SELECT * FROM SNOWFLAKE_SAMPLE_DATA.tpch_sf10.nation;
CREATE OR REPLACE TABLE dbt_tutorial_db.raw.orders   AS SELECT * FROM SNOWFLAKE_SAMPLE_DATA.tpch_sf10.orders;

ALTER TABLE dbt_tutorial_db.raw.nation   ADD COLUMN _updated_at TIMESTAMP; UPDATE dbt_tutorial_db.raw.nation   SET _updated_at = CURRENT_TIMESTAMP();
ALTER TABLE dbt_tutorial_db.raw.customer ADD COLUMN _updated_at TIMESTAMP; UPDATE dbt_tutorial_db.raw.customer SET _updated_at = CURRENT_TIMESTAMP();
ALTER TABLE dbt_tutorial_db.raw.orders   ADD COLUMN _updated_at TIMESTAMP; UPDATE dbt_tutorial_db.raw.orders   SET _updated_at = CURRENT_TIMESTAMP();