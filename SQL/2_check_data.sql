-----------------------------------------------------------------
-- RAW layer
-----------------------------------------------------------------
SELECT *
  FROM dbt_tutorial_db.raw.customer
  LIMIT 10;

SELECT *
  FROM dbt_tutorial_db.raw.nation
  LIMIT 10;

SELECT *
  FROM dbt_tutorial_db.raw.orders
  LIMIT 10;

SELECT *
  FROM dbt_tutorial_db.raw.region;


-----------------------------------------------------------------
-- PREP layer
-----------------------------------------------------------------
SELECT *
  FROM dbt_tutorial_db.prep.customer_dedupe_source
 LIMIT 100;

SELECT *
  FROM dbt_tutorial_db.prep.nation_dedupe_source
 LIMIT 100;

SELECT *
  FROM dbt_tutorial_db.prep.orders_dedupe_source
 LIMIT 100;

SELECT *
  FROM dbt_tutorial_db.prep.region_dedupe_source
 LIMIT 100;

-----------------------------------------------------------------
-- PROD layer
-----------------------------------------------------------------

SELECT *
  FROM dbt_tutorial_db.prod.sales_per_year;


SELECT *
  FROM dbt_tutorial_db.prod.orders_per_country;

SELECT *
  FROM dbt_tutorial_db.prod.top_customers;

        