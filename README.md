# dbt - from zero to hero in 90 minutes


**Abstract:** In this comprehensive tutorial, a GitLab Data veteran will show the power of Data Build Tool, also known as `dbt`, a rising star in the Data World. You will get the basic knowledge about a use case where, how and why to use `dbt`. This is an ideal chance to catch the best mutual venture for `SQL` and `Python`. 

Why this is important: whether you are a Data veteran or you just started your professional journey, this session will bring a smile to your face and expose you to how to leverage data transformation and make your life easier. `dbt` is a "swiss-knife" solution among Data Transformation tools.

If you like Open Source tools, Python, SQL and Data, this is the tutorial you shouldn't miss.


**Duration**: `90` min

Lecturer: [**Radovan Bacovic**](https://gitlab.com/rbacovic/rbacovic/-/blob/main/README.md)

Video material you should find [**here**](https://www.youtube.com/watch?v=Z9m6ljiYt_0).

---

## Intro 
**Duration**: `10` min

- What is `dbt`
- Architecture
- Plans (cli, cloud, open source)
- Purpose
- Plugins


## Installation
**Duration**: `10` min

- Install
    - Using `Poetry`: [Setting up dbt using Poetry — A clean and effective approach for your team](https://towardsdev.com/setting-up-dbt-using-poetry-a-clean-and-effective-approach-for-your-team-36c1da93ab77)
    - Install `Poetry` on your machine:
        ```bash
        curl -sSL https://install.python-poetry.org | python3 -
        ```
    - Install components from the `Poetry` definition file (`pyproject.toml`):
        ```bash
        poetry install
        ```
    - Test installation: 
        ```bash
        poetry run dbt --version
      
        # Core:
        #   - installed: 1.4.6
        #   - latest:    1.4.6 - Up to date!
        # 
        # Plugins:
        #   - snowflake: 1.4.2 - Up to date!

        ```

## Project setup

**Duration**: `10` min
- Create project. Use the [link](https://docs.getdbt.com/docs/quickstarts/dbt-core/manual-install)
    ```bash
    poetry run dbt init dbt_tutorial
    ``` 

Depending on the target database you want to use, you can set up `Snowflake` or `DuckDb` _(or anything you want)_.

### Option 1: `Snowflake` setup

<details><summary>Click to expand</summary>


- DB setup
    - Use [Snowflake Trial](https://signup.snowflake.com/) (`30` days of free usage)

- `dbt_project.yml` file
    ```sql
    profile: 'dbt_tutorial'
    ```

- `profiles.yml` file
    ```bash
    vi ~/.dbt/profiles.yml
    ```
    ```yml
    dbt_tutorial:
      target: default
      outputs:
        default:
          account: {your_account_address}
          database: {database}
          password: {password}
          role: {role}
          schema: {schema}
          threads: 1
          type: snowflake
          user: {username}
          warehouse: {warehouse}
    ```
  
</details>

### Option2: `duck db` setup

<details><summary>Click to expand</summary>

- `dbt_project.yml` file
    ```sql
    profile: 'dbt_tutorial_duck_db'
    ```

`vi ~/.dbt/profiles.yml`

- `profiles.yml` file
    ```bash
    dbt_tutorial_duck_db:
      target: dev
      outputs:
        dev:
          type: duckdb
          path: 'dbt_tutorial_db.duckdb'
          schema: raw
    ```

#### Schema initial setup

Run the script to create `duck_db` file. It is important to be in the root folder of the `dbt_tutorial` repository.

```sql
duckdb dbt_tutorial/dbt_tutorial_db.duckdb

CREATE OR REPLACE SCHEMA raw;
CREATE OR REPLACE SCHEMA prep;
CREATE OR REPLACE SCHEMA prod;

USE RAW;

CREATE OR REPLACE TABLE raw.nation   AS SELECT * FROM read_csv_auto('src_files/nation.csv', header=True);
CREATE OR REPLACE TABLE raw.customer AS SELECT * FROM read_csv_auto('src_files/customer.csv', header=True);
CREATE OR REPLACE TABLE raw.orders   AS SELECT * FROM read_csv_auto('src_files/orders.csv', header=True);

ALTER TABLE nation   ADD COLUMN _updated_at TIMESTAMP DEFAULT NOW();
ALTER TABLE customer ADD COLUMN _updated_at TIMESTAMP DEFAULT NOW();
ALTER TABLE orders   ADD COLUMN _updated_at TIMESTAMP DEFAULT NOW();

-- exit with CTRL-D
```

Check `RAW` schema:
```sql
D show tables;
┌──────────┐
│   name   │
│ varchar  │
├──────────┤
│ customer │
│ nation   │
│ orders   │
└──────────┘
```
</details>

- Config file `dbt_project.yml`
- Config global/local: 
    - Did you know that you can also configure models directly within `SQL` files? This will override configurations stated in `dbt_project.yml` file?
- Setup `Snowflake` or  `duckDb` database
- Test connection - this is the way you can run a dry run option for testing the entire setup `dbt` + `Snowflake`
    ```bash
    poetry run dbt debug
    ```
- Project structure

## Perstistence

**Duration**: `10` min

- Seeds
```bash
poetry run dbt seed
```

- Table
- Increment
- View

```bash
poetry run dbt run --models customer_dedupe_source

poetry run dbt run --models nation_dedupe_source

poetry run dbt run --models orders_dedupe_source

poetry run dbt run --models region_dedupe_source
```

- Full refresh
```bash
poetry run dbt run --models region_dedupe_source --full-refresh
```
- Testing

## Reference
**Duration**: `10` min

- Create a reference

## Schema
**Duration**: `10` min

- `source.yml` file
- `schema.yml` file
- Documentation
- Markdown - in the file `prep_doc.md` 
    - `schema.yml`
    ```yml
      - name: nation_dedupe_source
        description: '{{ doc("nation_dedupe_source") }}'
    ```

## Break
**Duration**: `10` min


## Documentation
**Duration**: `5` min

- Generate
```bash
poetry run dbt docs generate 
# generate documentation in yml file

poetry run dbt docs serve --port 8001
# expose documentation as a web page
```

![](dbt_tutorial/images/model_prep_nation.png)

- Lineage
![](dbt_tutorial/images/lineage.png)

## Macros
**Duration**: `10` min

- Code re-usability
- Debugging - for debugging look into folders: 
```bash
/target
    /compiled
    /run
```

Example of compiled and running models folder structure:

![](dbt_tutorial/images/debug.png)

## Run the project
**Duration**: `10` min

- Basic run models
```bash
poetry run dbt run
# will run all dbt models 
```

and check results: 
```bash
07:29:17  Running with dbt=1.5.0
07:29:17  Found 7 models, 9 tests, 0 snapshots, 0 analyses, 323 macros, 0 operations, 1 seed file, 8 sources, 0 exposures, 0 metrics, 0 groups
07:29:17  
07:29:21  Concurrency: 1 threads (target='dev')
07:29:21  
07:29:21  1 of 7 START sql incremental model prep.customer_dedupe_source ................. [RUN]
07:29:33  1 of 7 OK created sql incremental model prep.customer_dedupe_source ............ [SUCCESS 1500000 in 12.43s]
07:29:33  2 of 7 START sql incremental model prep.nation_dedupe_source ................... [RUN]
07:29:36  2 of 7 OK created sql incremental model prep.nation_dedupe_source .............. [SUCCESS 25 in 2.69s]
07:29:36  3 of 7 START sql incremental model prep.orders_dedupe_source ................... [RUN]
07:30:05  3 of 7 OK created sql incremental model prep.orders_dedupe_source .............. [SUCCESS 15000000 in 28.85s]
07:30:05  4 of 7 START sql view model prod.orders_per_country ............................ [RUN]
07:30:06  4 of 7 OK created sql view model prod.orders_per_country ....................... [SUCCESS 1 in 1.15s]
07:30:06  5 of 7 START sql table model prep.region_dedupe_source ......................... [RUN]
07:30:07  5 of 7 OK created sql table model prep.region_dedupe_source .................... [SUCCESS 1 in 1.21s]
07:30:07  6 of 7 START sql view model prod.sales_per_year ................................ [RUN]
07:30:08  6 of 7 OK created sql view model prod.sales_per_year ........................... [SUCCESS 1 in 0.76s]
07:30:08  7 of 7 START sql view model prod.top_customers ................................. [RUN]
07:30:09  7 of 7 OK created sql view model prod.top_customers ............................ [SUCCESS 1 in 1.06s]
07:30:09  
07:30:09  Finished running 3 incremental models, 3 view models, 1 table model in 0 hours 0 minutes and 51.75 seconds (51.75s).
07:30:09  
07:30:09  Completed successfully
07:30:09  
07:30:09  Done. PASS=7 WARN=0 ERROR=0 SKIP=0 TOTAL=7
```

- basic run tests
```bash
poetry run dbt test
# will run dbt tests for all models
```

and check results: 
```bash
07:28:40  Running with dbt=1.5.0
07:28:41  Found 7 models, 9 tests, 0 snapshots, 0 analyses, 323 macros, 0 operations, 1 seed file, 8 sources, 0 exposures, 0 metrics, 0 groups
07:28:41  
07:28:43  Concurrency: 1 threads (target='dev')
07:28:43  
07:28:43  1 of 9 START test accepted_values_region_dedupe_source_id__False__0__1__2__3__4  [RUN]
07:28:44  1 of 9 PASS accepted_values_region_dedupe_source_id__False__0__1__2__3__4 ...... [PASS in 0.96s]
07:28:44  2 of 9 START test not_null_nation_dedupe_source_id ............................. [RUN]
07:28:45  2 of 9 PASS not_null_nation_dedupe_source_id ................................... [PASS in 1.05s]
07:28:45  3 of 9 START test not_null_nation_dedupe_source_name ........................... [RUN]
07:28:46  3 of 9 PASS not_null_nation_dedupe_source_name ................................. [PASS in 0.57s]
07:28:46  4 of 9 START test not_null_orders_per_country_name ............................. [RUN]
07:28:47  4 of 9 PASS not_null_orders_per_country_name ................................... [PASS in 0.97s]
07:28:47  5 of 9 START test not_null_orders_per_country_rank ............................. [RUN]
07:28:49  5 of 9 PASS not_null_orders_per_country_rank ................................... [PASS in 1.92s]
07:28:49  6 of 9 START test not_null_region_dedupe_source_id ............................. [RUN]
07:28:50  6 of 9 PASS not_null_region_dedupe_source_id ................................... [PASS in 0.68s]
07:28:50  7 of 9 START test unique_nation_dedupe_source_name ............................. [RUN]
07:28:50  7 of 9 PASS unique_nation_dedupe_source_name ................................... [PASS in 0.65s]
07:28:50  8 of 9 START test unique_orders_per_country_name ............................... [RUN]
07:28:51  8 of 9 PASS unique_orders_per_country_name ..................................... [PASS in 0.92s]
07:28:51  9 of 9 START test unique_region_dedupe_source_id ............................... [RUN]
07:28:52  9 of 9 PASS unique_region_dedupe_source_id ..................................... [PASS in 0.58s]
07:28:52  
07:28:52  Finished running 9 tests in 0 hours 0 minutes and 10.78 seconds (10.78s).
07:28:52  
07:28:52  Completed successfully
07:28:52  
07:28:52  Done. PASS=9 WARN=0 ERROR=0 SKIP=0 TOTAL=9
```

- run specific model (or dependant models)
```bash
poetry run dbt run --models +top_customers
# will run upstream models

poetry run dbt run --models orders_dedupe_source+
# will run downstream models

poetry run dbt run --models +orders_dedupe_source+
# will run upstream and downstream models
```

- run using tags
```bash
poetry run dbt run --select tag:prod
# run models which contain tag='prod'
```

- Use `build` command instead of `run` The dbt build command will:
    - `run` models
    - `test` tests
    - `snapshot` snapshots
    - `seed` seeds
In DAG order, for selected resources or an entire project.
```bash
poetry run dbt build --models +orders_dedupe_source+
```

- Selector - avoid complex commands `dbt run ...`
    - create `selectors.yml` file 
    ```yml
    selectors:
    - name: prep_only
      description: "run prep layer only"
      definition:
        'tag:prep'
    ``` 
    - run the command
    ```bash
    poetry run dbt run --selector prep_only
    ```
  
  
- Models
- Testing (`schema.yml`)
```yml
  - name: region_dedupe_source
    description: Regions
    columns:
      - name: id
        tests:
          - unique
          - not_null
          - accepted_values:
              values: [0, 1, 2, 3, 4]
              quote: false
```

```bash
poetry run dbt test
```



## Automation
**Duration**: `10` min

- Cron
- Airflow
- Dagster

## Conclusion
**Duration**: `5` min

- What's next
- Resources


### Resources

* Tutorial [**presentation**](https://docs.google.com/presentation/d/1ChSH1eipBk-Xy4ksD7FwiNu78RhWiivPLrSQme4FgdY/edit#slide=id.g215faee09db_0_0)
* `dbt` [official site](https://www.getdbt.com/)
* `dbt` [documentation](https://docs.getdbt.com/) 
* GitLab `dbt` project
    * GitLab `dbt` [project ](https://gitlab.com/gitlab-data/analytics/-/tree/master/transform)
    * GitLab `dbt` [documentation](https://dbt.gitlabdata.com/#!/model/model.gitlab_snowflake.version_usage_data_unpacked)
    
