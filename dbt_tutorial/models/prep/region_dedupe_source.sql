{{ config({
    "materialized": "table",
    "unique_key": "id"
    })
}}

WITH formatted AS (SELECT r_regionkey::INTEGER   AS id,
                          UPPER(r_name)::varchar AS name,
                          r_comment::varchar     AS comment
                   FROM {{ source('raw', 'region') }}
)
SELECT *
  FROM formatted

