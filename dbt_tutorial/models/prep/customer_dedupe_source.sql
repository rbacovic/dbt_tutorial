{{ config({
    "materialized": "incremental",
    "unique_key": "id"
    })
}}

WITH formatted AS (SELECT c_custkey::INTEGER     AS id,
                          c_name::VARCHAR        AS name,
                          c_address::VARCHAR     AS address,
                          c_nationkey::INTEGER   AS nation_key,
                          c_phone::VARCHAR       AS phone,
                          c_acctbal::INTEGER     AS acctbal,
                          c_mktsegment::VARCHAR  AS mktsegment,
                          c_comment::VARCHAR     AS comment,
                          _updated_at::TIMESTAMP AS _updated_at
                   FROM  {{ source('raw', 'customer') }}
)
SELECT *
  FROM formatted
{% if is_incremental() %}

WHERE _updated_at >= (SELECT MAX(_updated_at) FROM {{this}})

{% endif %}
QUALIFY ROW_NUMBER() OVER (PARTITION BY id ORDER BY _updated_at DESC) = 1

