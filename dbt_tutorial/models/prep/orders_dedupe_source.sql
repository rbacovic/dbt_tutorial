{{ config({
    "materialized": "incremental",
    "unique_key": "id"
    })
}}

WITH formatted AS (SELECT o_orderkey::INTEGER      AS id,
                          o_custkey::INTEGER       AS customer_id,
                          o_orderstatus::varchar   AS status,
                          o_totalprice::INTEGER    AS total,
                          o_orderdate::date        AS order_date,
                          o_orderpriority::varchar AS order_priority,
                          o_clerk::varchar         AS clerk,
                          o_shippriority::INTEGER  AS ship_prioriy,
                          o_comment::varchar       AS comment ,
                          _updated_at::TIMESTAMP   AS _updated_at
                   FROM {{ source('raw', 'orders') }}
)
SELECT *
  FROM formatted
{% if is_incremental() %}

WHERE _updated_at >= (SELECT MAX(_updated_at) FROM {{this}})

{% endif %}
QUALIFY ROW_NUMBER() OVER (PARTITION BY id ORDER BY _updated_at DESC) = 1

