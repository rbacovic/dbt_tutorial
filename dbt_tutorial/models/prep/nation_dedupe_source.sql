{{ config({
    "materialized": "incremental",
    "unique_key": "id"
    })
}}

WITH formatted AS (SELECT n_nationkey::INTEGER   AS id,
                          n_name::VARCHAR        AS name,
                          n_regionkey::INTEGER   AS region_key,
                          n_comment::VARCHAR     AS comment,
                          _updated_at::TIMESTAMP AS _updated_at
                   FROM {{ source('raw', 'nation') }}
)
SELECT *
  FROM formatted
{% if is_incremental() %}

WHERE _updated_at >= (SELECT MAX(_updated_at) FROM {{this}})

{% endif %}
QUALIFY ROW_NUMBER() OVER (PARTITION BY id ORDER BY _updated_at DESC) = 1

