WITH orders_per_country AS (
    SELECT customer_id,
           SUM(total) AS total
      FROM {{ source('prep', 'orders_dedupe_source') }}
     GROUP BY customer_id
), customer AS (
    SELECT id, name, nation_key
      FROM {{ source('prep', 'customer_dedupe_source') }}
), nation AS (
    SELECT id, name, region_key
      FROM {{ source('prep', 'nation_dedupe_source') }}
), sum_per_country AS (
    SELECT nation.name,
           SUM(total) total
      FROM orders_per_country
      LEFT JOIN customer
        ON orders_per_country.customer_id = customer.id
      LEFT JOIN nation
        ON customer.nation_key = nation.id
        GROUP BY nation.name
)
SELECT rank() OVER (PARTITION BY NULL ORDER BY total DESC)  rank,
       sum_per_country.name,
       sum_per_country.total
  FROM sum_per_country
  ORDER BY sum_per_country.total DESC
  LIMIT 10