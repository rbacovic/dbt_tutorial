{{ config({
    "materialized": "table",
    "unique_key": "rank"
    })
}}
WITH sales_per_year AS (
SELECT clerk,
       YEAR(order_date)::VARCHAR AS order_year,
       SUM(total)                AS total,
       COUNT(1)                  AS order_cnt
  FROM {{ source('prep', 'orders_dedupe_source') }}
 GROUP BY clerk,
          YEAR(order_date)
)
SELECT rank() OVER (PARTITION BY 1 ORDER BY total DESC)  rank,
       clerk,
       order_year,
       total,
       order_cnt,
       (ROUND(total/order_cnt,2))::FLOAT AS average_per_order
  FROM sales_per_year
  ORDER BY total DESC
  LIMIT 5
