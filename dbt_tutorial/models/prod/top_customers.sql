
{{ simple_cte([
    ('customer', 'customer_dedupe_source'),
    ('nation', 'nation_dedupe_source'),
    ('orders', 'orders_dedupe_source')
]) }}

, joined AS (SELECT customer.name::VARCHAR          AS name,
                    nation.name::VARCHAR            AS nation,
                    IFNULL(orders.total::INTEGER,0) AS total
                    --orders.total::INTEGER           AS total -- fix this to show proper ordering
   FROM customer
   LEFT JOIN nation
     ON customer.nation_key = nation.id
   LEFT JOIN orders
     ON customer.id = orders.customer_id
    )
, final AS (SELECT name,
                   nation,
                   SUM(total) AS total
              FROM joined
          GROUP BY name,
                   nation)

SELECT *
  FROM final
ORDER BY total DESC
LIMIT 10
