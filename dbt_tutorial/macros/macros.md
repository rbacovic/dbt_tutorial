{% docs simple_cte %}
Used to simplify CTE imports in a model.

A large portion of import statements in a SQL model are simple `SELECT * FROM table`. Writing pure SQL is verbose and this macro aims to simplify the imports.

The macro accepts once argument which is a list of tuples where each tuple has the alias name and the table reference.



{% enddocs %}